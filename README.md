# Vim folder

A repository to ease syncronizing multiple vim installations.

## Usage:

General installation:
```bash
git clone git@bitbucket.org:skeen/vim-folder.git
mv vim-folder/.* .
rm -rf vim-folder
```

Plugins:
```bash
# Open vim and run
:PlugInstall
```
